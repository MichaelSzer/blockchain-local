const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const Blockchain = require('./blockchain');
const bitcoin = new Blockchain();
const uuid = require('uuid/v1');
const axios = require('axios');

const nodeAddress = uuid().split('-').join('');

const port = process.argv[2];		// Get Port from console argument

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/blockchain', (req, res) => {
	res.send(bitcoin);
});

app.post('/transaction/broadcast', (req, res) => {
	const { amount, sender, recipient } = req.body;

	// Check for required properties
	if(typeof(amount) === "undefined" || typeof(sender) === "undefined" || typeof(recipient) === "undefined"){
		res.status(400).send('/transaction/broadcast must have: amount, sender, recipient.');
		return;
	}

	const newTransaction = bitcoin.createNewTransaction(amount, sender, recipient);

	bitcoin.addTransactionToPendingTrasactions(newTransaction);

	const postTransactionsPromises = [];
	bitcoin.networkNodes.forEach( networkNodeUrl => {
		const postTransactionConfig = {
			url: networkNodeUrl + '/transaction',
			method: 'POST',
			data: { newTransaction }
		}

		postTransactionsPromises.push(axios(postTransactionConfig));
	});

	Promise.all(postTransactionsPromises).then( data => {

		res.json({ note: 'Transaction created and broadcast successfully.' });
	})
	.catch(err => { 
		console.log(err);
		res.status(400).send('Error.');
	});
});

app.post('/transaction', (req, res) => {

	if(typeof(req.body.newTransaction) === 'undefined'){
		res.status(400).send('Transaction must have the data in body-->newTransaction.');
		return;
	}
	
	const {amount, sender, recipient} = req.body.newTransaction;

	// Check for required properties
	if(typeof(amount) === "undefined" || typeof(sender) === "undefined" || typeof(recipient) === "undefined"){
		res.status(400).send('Transaction must have: amount, sender, recipient.');
		return;
	}

	const blockIndex = bitcoin.addTransactionToPendingTrasactions(req.body.newTransaction);
	res.json({ 	transactionDetails: {
					amount,
					sender,
					recipient
				},
				transactionLocation: `Transaction will be added in block ${blockIndex}.` });
});


app.get('/mine', (req, res) => {

	const lastBlock = bitcoin.getLastBlock();
	const previousBlockHash = lastBlock['hash'];
	const currentBlockData = {
		transactions: bitcoin.pendingTransactions,
		index: lastBlock['index'] + 1
	};
	const nonce = bitcoin.proofOfWork(previousBlockHash, currentBlockData);
	const blockHash = bitcoin.hashBlock(previousBlockHash, currentBlockData, nonce);
	const newBlock = bitcoin.createNewBlock(nonce, previousBlockHash, blockHash);
	bitcoin.addNewBlock(newBlock);

	const postNewBlockPromises = [];
	bitcoin.networkNodes.forEach( networkNodeUrl => {
		const postNewBlockConfig = {
			method: 'POST',
			url: networkNodeUrl + '/receive-new-block',
			data: { newBlock }
		};

		postNewBlockPromises.push(axios(postNewBlockConfig));
	});

	Promise.all(postNewBlockPromises).then( data => {
		const reqMiningRewardConfig = {
			method: 'POST',
			url: bitcoin.currentNodeUrl + '/transaction/broadcast',
			data: { 
				amount: 12.5,
				sender: '00',
				recipient: nodeAddress
			}
		};

		return axios(reqMiningRewardConfig);
	})
	.then( data => {
		res.json({
			note: "New block mined & broadcast successfully!",
			block: newBlock
		});
	})
	.catch(err => {
		console.log(err);
		res.status(400).send('Error.');
	});
});


app.post('/receive-new-block', (req, res) => {
	const { newBlock } = req.body;

	// Check for required properties
	if(typeof(newBlock) === "undefined"){
		res.status(400).send('/receive-new-block must have: newBlock.');
		return;
	}

	const lastBlock = bitcoin.getLastBlock();
	const correctHash = lastBlock.hash === newBlock.previousBlockHash;
	const correctIndex = lastBlock['index'] + 1 === newBlock['index'];

	if(correctHash && correctIndex){
		
		bitcoin.addNewBlock(newBlock);
		res.json({ 	note: 'New block received and accepted.',
					newBlock });

	} else {
		res.status(400).send('Illegitimate block. The new block was rejected.');
	}
});

// register a node and broadcast to the entire network
app.post('/register-and-broadcast-node', (req, res) => {
	const { newNodeUrl } = req.body;

	// Check for required properties
	if(typeof(newNodeUrl) === "undefined"){
		res.status(400).send('Register and broadcast node must have: newNodeUrl.');
		return;
	}

	if(bitcoin.networkNodes.indexOf(newNodeUrl) == -1)
		bitcoin.networkNodes.push(newNodeUrl);

	regNodesPromises = [];
	bitcoin.networkNodes.forEach(networkNodeUrl => {

		// '/register-node'
		const requestConfig = {
			url: networkNodeUrl + '/register-node',
			method: 'POST',
			data: { newNodeUrl }
		};

		regNodesPromises.push(axios(requestConfig));
	});

	Promise.all(regNodesPromises).then( data => {

		// '/request-nodes-bulk'
		const bulkRegistrationOptions = {
			url: newNodeUrl + '/register-nodes-bulk',
			method: 'POST',
			data: { allNetworkNodes: [ ...bitcoin.networkNodes, bitcoin.currentNodeUrl ] }
		};

		return axios(bulkRegistrationOptions);
	})
	.then( data => {
		res.json({ note: 'New node registered with network successfully.' });
	})
	.catch( err => console.log(err) );
});


// register a node with the network
app.post('/register-node', (req, res) => {
	const { newNodeUrl } = req.body;

	// Check for required properties
	if(typeof(newNodeUrl) === "undefined"){
		res.status(400).send('Register node must have: newNodeUrl.');
		return;
	}
	
	if(bitcoin.networkNodes.indexOf(newNodeUrl) == -1 && bitcoin.currentNodeUrl !== newNodeUrl)
		bitcoin.networkNodes.push(newNodeUrl);

	res.json({ note: 'New node registered successfully with node.' });
});


// register list of nodes
app.post('/register-nodes-bulk', (req, res) => {
	const { allNetworkNodes } = req.body;

	// Check for required properties
	if(typeof(allNetworkNodes) === "undefined"){
		res.status(400).send('Register nodes bulk must have: allNetworkNodes.');
		return;
	}

	allNetworkNodes.forEach( networkNodeUrl => {

		if( bitcoin.networkNodes.indexOf(networkNodeUrl) == -1 &&
			networkNodeUrl !== bitcoin.currentNodeUrl )
			bitcoin.networkNodes.push(networkNodeUrl);
	});	

	res.json({ note: 'Bulk registration successful.' });
});

app.get('/consensus', function(req, res){

	const reqPromisesBlockchain = []
	bitcoin.networkNodes.forEach( networkNodeUrl => {
		const getBlockchainConf = {
			url: networkNodeUrl + '/blockchain',
			method: 'GET'
		}

		reqPromisesBlockchain.push(axios(getBlockchainConf));
	});

	Promise.all(reqPromisesBlockchain).then( blockchains => {

		const currentChainLength = bitcoin.chain.length;
		let maxChainLength = currentChainLength;
		let newLongestChain = null;
		let newPendingTransactions = null;
		blockchains.forEach( blockchain => {
			if(blockchain.data.chain.length > maxChainLength){
				maxChainLength = blockchain.data.chain.length;
				newLongestChain = blockchain.data.chain;
				newPendingTransactions = blockchain.data.pendingTransactions;
			}
		});

		if(!newLongestChain || (newLongestChain && !bitcoin.chainIsValid(newLongestChain))){
			res.status(400).json({ 
				note: 'Current chain has not been replaced.',
				chain: bitcoin.chain
			});
		}else{
			bitcoin.chain = newLongestChain;
			bitcoin.pendingTransactions = newPendingTransactions;

			res.json({
				note: 'This chain has been replaced.',
				chain: bitcoin.chain
			});
		}
	});
});


app.get('/block/:blockHash', function(req, res){

	const { blockHash } = req.params;
	const block = bitcoin.getBlock(blockHash.toString());

	if(block === null)
		res.json({ 	note: `Block wasn't found`,
					blockHash: blockHash.toString() });
	else
		res.json({ 	note: 'Block was found successfully.',
					block })
});

app.get('/transaction/:transactionId', function(req, res){

	const { transactionId } = req.params;
	const { transaction, block } = bitcoin.getTransaction(transactionId);

	if(transaction === null)
		res.json({ 	note: `Transaction wasn't found.`,
					transactionId });
	else
		res.json({	note: `Transaction found.`,
					transaction,
					block });
});

app.get('/address/:address', function(req, res){

	const { address } = req.params;
	const addressData = bitcoin.getAddressData(address);

	if(addressData.transactions.length === 0)
		res.json({ 	note: `Transactions weren't found.`,
					address });
	else
		res.json({ 	note: `Transactions were found.`,
					...addressData });
});

app.get('/block-explorer', function(req, res){
	res.sendFile('./block-explorer/index.html', { root: __dirname });
});

app.listen(port, () => {
	console.log(`Listening on port ${port}.`);
});