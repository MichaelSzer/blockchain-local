const sha256 = require('sha256');
const currentNodeUrl = process.argv[3];		// Get node Uri
const uuid = require('uuid/v1');

function Blockchain(){
	this.chain = [];	// Chain with blocks
	this.pendingTransactions = [];	// Pending Transactions
	this.difficulty = 4;

	this.currentNodeUrl = currentNodeUrl;
	this.networkNodes = [];

	const genesisBlock = this.createNewBlock(777, '0', 'GENESIS_BLOCK');
	this.addNewBlock(genesisBlock);
}

Blockchain.prototype.createNewBlock = function(nonce, previousBlockHash, hash){
	const newBlock = {
		index: this.chain.length+1,
		timestamp: Date.now(),
		transactions: this.pendingTransactions,
		nonce: nonce, 	// Proof of work
		hash: hash,
		previousBlockHash: previousBlockHash
	};

	return newBlock;
}

Blockchain.prototype.addNewBlock = function(newBlock){

	this.pendingTransactions = [];
	this.chain.push(newBlock);

	return newBlock;
}

Blockchain.prototype.getLastBlock = function(){
	return this.chain[this.chain.length-1];
}

Blockchain.prototype.createNewTransaction = function(amount, sender, recipient){
	const newTransaction = {
		amount,
		sender,
		recipient,
		transactionId: uuid().split('-').join('')
	};

	return newTransaction;
}

Blockchain.prototype.addTransactionToPendingTrasactions = function(transaction){
	
	this.pendingTransactions.push(transaction);

	return this.getLastBlock()['index'] + 1;
};

Blockchain.prototype.hashBlock = function(previousBlockHash, currentBlockData, nonce){

	const dataAsString = previousBlockHash + nonce.toString() + JSON.stringify(currentBlockData);
	const hash = sha256(dataAsString);

	return hash;
}

Blockchain.prototype.checkHash = function(hash){
	for(let i = 0; i < this.difficulty; i++) {
		if(hash[i] !== '0'){
			return false;
		}
	}

	return true;
}

Blockchain.prototype.proofOfWork = function(previousBlockHash, currentBlockData){

	let nonce = 0;
	let hash = this.hashBlock(previousBlockHash, currentBlockData, nonce);

	while (true) {

		if(this.checkHash(hash))
			break;

		nonce++;
		hash = this.hashBlock(previousBlockHash, currentBlockData, nonce);		
	}

	return nonce;
}

Blockchain.prototype.chainIsValid = function(blockchain){

	// Check hashes
	for(let i = 1; i < blockchain.length; i++){
		const currentBlock = blockchain[i];
		const previousBlock = blockchain[i-1];
		const blockHash = this.hashBlock(previousBlock.hash, { transactions: currentBlock.transactions, index: currentBlock.index }, currentBlock.nonce);

		if(currentBlock.previousBlockHash !== previousBlock.hash || !this.checkHash(blockHash)){ 
			return false;
		}
	}

	const genesisBlock = blockchain[0];
	const correctNonce = genesisBlock.nonce === 777;
	const correctPrevBlockHash = genesisBlock.previousBlockHash === '0';
	const correctHash = genesisBlock.hash === 'GENESIS_BLOCK';
	const correctTransactions = genesisBlock.transactions.length === 0;


	const correctGenesisBlock = correctHash && correctPrevBlockHash && correctHash && correctTransactions;
	if(!correctGenesisBlock) return false;

	return true;
}


Blockchain.prototype.getBlock = function(blockHash){
	let foundBlock = null;
	this.chain.forEach( block => {
		if(block.hash === blockHash) foundBlock = block;
	});
	return foundBlock;
}


Blockchain.prototype.getTransaction = function(transactionId){
	let foundTransaction = null;
	let foundBlock = null;

	this.chain.forEach( block => {
		block.transactions.forEach( transaction => {
			if(transaction.transactionId === transactionId){
				foundTransaction = transaction;
				foundBlock = block;
			}
		});
	});
	return { transaction: foundTransaction, block: foundBlock };
}

Blockchain.prototype.getAddressData = function(address){

	const addressTransactions = [];
	let balance = 0;

	this.chain.forEach( block => {
		block.transactions.forEach( transaction => {
			if(transaction.sender === address){
				balance -= transaction.amount;
				addressTransactions.push(transaction);
			}else if(transaction.recipient === address){
				balance += transaction.amount;
				addressTransactions.push(transaction);
			}
		});
	});

	return { transactions: addressTransactions, balance  };
}

module.exports = Blockchain;